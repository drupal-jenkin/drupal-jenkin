/**
 * Scripts
 */
(function ($) {
  Drupal.behaviors.ghme = {
    attach: function (context) {
      'use strict';

      // Global variables
      var $body = $('body', context),
      $searchBtn = $('.search-ico, .search-close', context),
      $searchField = $('input.form-search', context);

      /**
       * Sticky Header
       */
      stk_hdr();
      $(window).scroll(function() {
        stk_hdr();
      });

      /**
       * Search Action
       */
      $searchBtn.on('click', function() {
        var $this = $(this);
        $body.toggleClass('search-enabled');
        if($this.hasClass('search-ico')) {
          $searchField.focus();
        }
      });

      $(document).ready(function() {
        /**
         * Language Swithcher
         */
        $('.sel-language').remove();
        var $langBlock = $('.block-language');
        $langBlock.prepend('<div class="sel-language">' + $langBlock.find('li.is-active').text() + '<i class="fas fa-angle-down ml-2"></i></div>');

        /**
         * Right Filters
         */
        $('.rg-fltr .form-type-select').on("click", ".chosen-single", function() {
          $(this).closest('.form-type-select').addClass('fltr-enabled');
        });
        $('.rg-fltr .form-type-select').on("click", "label", function() {
          $(this).closest('.form-type-select').toggleClass('fltr-enabled');
        });
        $('.rg-fltr').on("click", ".chosen-results li", function() {
          $(this).closest('.chosen-results').find('li').removeClass('result-selected');
          $(this).addClass('result-selected');
        });
        $('.rg-fltr .form-type-select').on('change', "select.form-select", function() {
          $(this).closest('.views-exposed-form').find('.form-submit').click();
        });
        
      });

      /**
       * User Block
       */
      var $userBlock = $('.user-block', context);
      if($userBlock.length) {
        var $accountMenu = $('.menu--account', context);
        $userBlock.prependTo($accountMenu);
      }

      /**
      * Sticky Header
      */
      function stk_hdr() {
        $(window).scrollTop() > 100 ? $body.addClass('w-scrd') : $body.removeClass('w-scrd');
      }
    }
  }
})(jQuery);